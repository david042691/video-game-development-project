﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bodymovement : MonoBehaviour {

    //private Rigidbody rb;
    private Animator animator;
    public float speed;
    public Transform Waist;
    float yaw;
    float pitch;
    // Use this for initialization
    void Start() {
      //  rb = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
    }
    void Update()    // Update is called once per frame
    {
     
    }


	// Update is called once per frame
	void FixedUpdate () {
        // Body rotation via mouse
        gameObject.transform.Rotate(new Vector3(0, Input.GetAxis("Mouse X"),0));
        Waist.Rotate( new Vector3(0, 0,-Input.GetAxis("Mouse Y")));
        //yaw += Input.GetAxis("Mouse X") * 4.0f;
        //pitch -= Input.GetAxis("Mouse Y") * 4.0f;
        //transform.eulerAngles = new Vector3(pitch, yaw, 0f);

        //movement WASD
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        transform.position += Vector3.forward * moveVertical;
        transform.position -= Vector3.left * moveHorizontal;
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        //rb.AddForce(movement * speed);

        //add animation
      animator.SetBool("Movement", movement.sqrMagnitude >0);
    }
}
