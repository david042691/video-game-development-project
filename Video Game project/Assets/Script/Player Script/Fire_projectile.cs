﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire_projectile : MonoBehaviour
{
    private Animator animator;
    public float speed;
    public GameObject bulletPrefab;
    public bool gunup;
    public ParticleSystem myEffect;
    public AudioSource mySound;
    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire2"))
        {
            gunup = !gunup;
            animator.SetBool("Gunup", gunup);
        }

        if (Input.GetButtonDown("Fire1") && gunup)
          {
            
                //make bullet
                GameObject newBullet = GameObject.Instantiate(bulletPrefab);

                //position
                // This script is on the hand so gameObject is LeftHand, not the whole dood.
                GameObject Hand = gameObject.transform.Find("Player/Upper_Body/Left_Arm/Left_Joint/Left_arm/Left_hand/gun/Cube").gameObject;
                newBullet.transform.position = Hand.transform.position;
                newBullet.transform.rotation = Hand.transform.rotation;

            //Make Smoke
         myEffect.Play();
            // Play sound
            mySound.Play();   
            //force
                Rigidbody bulletBody = newBullet.GetComponent<Rigidbody>();
			bulletBody.AddForce (Hand.transform.right * -1 * speed, ForceMode.Impulse);
         }
		 
	}
}