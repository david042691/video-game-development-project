﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HUD_Script : MonoBehaviour {
    public Text watertext;
    public int watercount;
    Watercountdown ThePlayerwater;

	// Use this for initialization
	void Start () {
        GameObject FindPlayer = GameObject.FindGameObjectWithTag("Player");
        ThePlayerwater = FindPlayer.GetComponent<Watercountdown>();
	}
	
	// Update is called once per frame
	void Update () {
        watercount = ThePlayerwater.Watercount;
        setwatertext();
	}

    public void setwatertext()
    {
        watertext.text = watercount.ToString();

    }

}
