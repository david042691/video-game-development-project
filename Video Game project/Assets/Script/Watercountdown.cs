﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Watercountdown : MonoBehaviour {
    public int Watercount=100;
    float Countdown;
    public float Timer;

	// Use this for initialization
	void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
        Countdown -= Time.deltaTime;
        if (Countdown <= 0)
        {
            Watercount -= 10;
            Countdown = Timer;
        }

        if (Watercount<=0)
        {
            SceneManager.LoadScene("Losingscene");
        }

    }
}
