﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class destroythings : MonoBehaviour {
    float Timer=15;
    GameManager GM;
	// Use this for initialization
	void Start () {
        GameObject TheGM = GameObject.FindGameObjectWithTag("GameManager");
        GM = TheGM.GetComponent<GameManager>();
	}
	
	// Update is called once per frame
	void Update () {
        Timer -= Time.deltaTime;
        if (Timer<=0)
        {
            Destroy(gameObject);
        }
	}

    public void OnCollisionEnter(Collision collision)
    {
        GameObject Hit = collision.gameObject;

        if (Hit.CompareTag("Enemy"))
        {
            GameObject.Destroy(Hit);
            Destroy(gameObject);
            
            GM.enemynumber -= 1;
            GM.enemydeadcount += 1;
        }
    }
}
