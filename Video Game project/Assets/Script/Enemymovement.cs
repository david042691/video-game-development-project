﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemymovement : MonoBehaviour
{
    GameObject Target;
    private NavMeshAgent agent;
    public float walkRadius;
    public float countdown;
    public float AttackSpeed;
    Rigidbody rb;
    bool Seeplayer= false;
    public Transform ThePlayer;

    // Use this for initialization
    void Start()
    {
        Target = GameObject.FindGameObjectWithTag("Player");
        agent = GetComponent<NavMeshAgent>();
        rb = GetComponent<Rigidbody>();
        ThePlayer = Target.transform;
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit result;

        Vector3 Targetposition = Target.transform.position - gameObject.transform.position;
        Targetposition.Normalize();
        Vector3 myForward = gameObject.transform.forward;
        float NPCSight = Vector3.Dot(myForward, Targetposition);

        if (NPCSight > 0.525)
        {
            Debug.Log("Facing Player");

            Physics.Raycast(transform.position, Targetposition, out result);
            GameObject objecthit = result.rigidbody.gameObject;
            Debug.DrawRay(transform.position, Targetposition * 100);
            if (objecthit.tag == ("Player"))
            {
                agent.destination = ThePlayer.position;
                Seeplayer = true;
            }
            else Seeplayer = false;



            countdown -= Time.deltaTime;
            if (countdown <= 0 && Seeplayer == false)
            {
                Vector3 randomDirection = transform.position + Random.insideUnitSphere * walkRadius;
                //randomDirection += transform.position;
                //NavMeshHit hit;
                agent.destination = randomDirection;
                //Vector3 finalPosition = hit.position;
                countdown = 2;
            }
        }
    }
}

