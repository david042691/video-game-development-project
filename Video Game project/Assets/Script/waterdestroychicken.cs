﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class waterdestroychicken : MonoBehaviour {
    GameManager GM;
    Watercountdown Playerwater;

    // Use this for initialization
    void Start () {
      GameObject  TheGM = GameObject.FindGameObjectWithTag("GameManager");
        GM = TheGM.GetComponent<GameManager>();
        GameObject FindPlayer = GameObject.FindGameObjectWithTag("Player");
        Playerwater = FindPlayer.GetComponent<Watercountdown>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnCollisionEnter(Collision collision)
    {
        GameObject Hit = collision.gameObject;
       
        if (Hit.CompareTag("Enemy"))
        {
            GameObject.Destroy(Hit);
            GM.enemynumber-= 1;
        }

        if(Hit.CompareTag("Player"))
        {
            Playerwater.Watercount = 100;
        }
    }
}
