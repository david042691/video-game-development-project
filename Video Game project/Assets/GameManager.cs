﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    public GameObject enemyprefab;
    public int enemynumber;
    float spawntime;
    public float Maxtime;
    GameObject EnemySpawnpoint;
    public float Enemykilled;
    public float enemydeadcount;

	// Use this for initialization
	void Start () {
        spawntime = Maxtime;
        EnemySpawnpoint = gameObject;
	}
	
	// Update is called once per frame
	void Update () {
        spawntime -= Time.deltaTime;

        if (spawntime <=0)
        {
            GameObject EnemySpawn = GameObject.Instantiate(enemyprefab);
            EnemySpawn.transform.position = EnemySpawnpoint.transform.position;
            spawntime = Maxtime;
            enemynumber += 1;
        }

        if (enemydeadcount>=Enemykilled)
        {
            SceneManager.LoadScene("Endscene");
        }
	}
}
